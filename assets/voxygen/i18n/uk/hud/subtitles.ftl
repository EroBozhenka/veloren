subtitle-campfire = Вогнище потріскує
subtitle-bird_call = Птахи співають
subtitle-bees = Бджоли дзижчать бззз
subtitle-owl = Сова укає
subtitle-running_water = Шум води
subtitle-lightning = Грім

subtitle-footsteps_grass = Кроки по траві
subtitle-footsteps_earth = Кпроки по землі
subtitle-footsteps_rock = Кроки по камінню
subtitle-footsteps_snow = Кроки по снігу
subtitle-pickup_item = Підняття предмета
subtitle-pickup_failed = Падіння предмета

subtitle-glider_open = Глайдер споряджено
subtitle-glider_close = Глайдер не споряджений
subtitle-glide = Планерування
subtitle-roll = Перекочування
subtitle-swim = Плавання
subtitle-climb = Сходження
subtitle-damage = Пошкоджння
subtitle-death = Смерть

subtitle-wield_bow = Лук споряджено
subtitle-unwield_bow = Лук не споряджений
subtitle-pickup_bow = Лук підібрано

subtitle-wield_sword = Меч споряджено
subtitle-unwield_sword = Меч не споряджений
subtitle-sword_attack = Замахнувся мечем
subtitle-pickup_sword = Меч підібрано

subtitle-wield_axe = Сокира споряджена
subtitle-unwield_axe = Сокира не споряджена
subtitle-axe_attack = Замахнувся сокирою
subtitle-pickup_axe = Сокира підібрана

subtitle-wield_hammer = Молот соряджено
subtitle-unwield_hammer = Молот не споряджений
subtitle-hammer_attack = Замахнувся молотом
subtitle-pickup_hammer = Молот підібрано

subtitle-wield_staff = Посох споряджено
subtitle-unwield_staff = Посох не споряджений
subtitle-fire_shot = Вистріл посохом
subtitle-staff_attack = Атака посохом
subtitle-pickup_staff = Посох підібрано

subtitle-wield_sceptre = Скіпетр споряджено
subtitle-unwield_sceptre = Скіпетр не споряджений
subtitle-sceptre_heal = Скіпетр випромінює ауру відновлення
subtitle-pickup_sceptre = Скіпетр підібрано

subtitle-wield_dagger = Кинджал споряджено
subtitle-uwield_dagger = Кинджал не споряджений
subtitle-dagger_attack = Замах кинджалом
subtitle-pickup_dagger = Кинджал підібрано

subtitle-wield_shield = Щит споряджено
subtitle-unwield_shield = Щит не соряджений
subtitle-shield_attack = Штовхнання щитом
subtitle-pickup_shield = Щит підібрано

subtitle-pickup_pick = Кайло підібрано
subtitle-pickup_gemstone = Коштовний камінь підібрано

subtitle-instrument_organ = Гра на Органі

subtitle-wield_instrument = Інструмент споряджено
subtitle-unwield_instrument = Інструмент не соряджений
subtitle-instrument_double_bass = Гра на Котрабасі
subtitle-instrument_flute = Гра на Флейті
subtitle-instrument_glass_flute = Гра на склянній Флейті
subtitle-instrument_lyre = Гра на Лірі
subtitle-instrument_icy_talharpa = Гра на Льодяній Талхарпі
subtitle-instrument_kalimba = Гра на Калімбі
subtitle-instrument_melodica = Гра на Піаніці
subtitle-instrument_lute = Гра на Лютні
subtitle-instrument_sitar = Гра на Ситарі
subtitle-instrument_guitar = Гра на Гітарі
subtitle-instrument_dark_guitar = Гра на темній Гітарі
subtitle-instrument_washboard = Гра на дошці для прання
subtitle-instrument_wildskin_drum = Гра на Барабані з дикої шкіри
subtitle-pickup_instrument = Інструмент підібрано

subtitle-explosion = Вибух

subtitle-arrow_shot = Випущено стрілу
subtitle-arrow_miss = Прмах стріли
subtitle-arrow_hit = Попадання стрілою
subtitle-skill_point = Отримано очко навички
subtitle-sceptre_beam = Промінь скіпетра
subtitle-flame_thrower = Вогнемет
subtitle-break_block = Зруйновано блок
subtitle-attack_blocked = Атаку заблоковано
subtitle-parry = Відбито
subtitle-interrupted =  Перервано
subtitle-stunned = Оглушено
subtitle-dazed = Збентежено
subtitle-knocked_down = Повалено

subtitle-attack-ground_slam = Удар по землі
subtitle-attack-laser_beam = Лазерний промінь
subtitle-attack-cyclops_charge = Циклоп атакує
subtitle-giga_roar = Рев морозного гіганта
subtitle-deep_laugh =  Глибокий сміх
subtitle-attack-flash_freeze = Блискавична заморозка
subtitle-attack-icy_spikes = Крижані шипи
subtitle-attack-ice_crack = Тріщина в льоді
subtitle-attack-steam = Пар

subtitle-consume_potion = Випиває зілля
subtitle-consume_apple = Гризе яблуко
subtitle-consume_cheese = Наминає ковалок Сиру
subtitle-consume_food = Харчується
subtitle-consume_liquid = Втамовує спрагу

subtitle-utterance-alligator-angry = Алігатор шипить
subtitle-utterance-antelope-angry = Антилопа фуркає
subtitle-utterance-biped_large-angry = Важке рохкання
subtitle-utterance-bird-angry = Пташиний вереск
subtitle-utterance-adlet-angry = Гавкіт Адлета
subtitle-utterance-pig-angry = Свиня рохкає
subtitle-utterance-reptile-angry = Рептилія шипить
subtitle-utterance-sea_crocodile-angry = Морський крокодил шипить
subtitle-utterance-saurok-angry = Саурок шипить
subtitle-utterance-cat-calm = Кіт мявчить
subtitle-utterance-cow-calm = Корова мукає
subtitle-utterance-fungome-calm = Грибоподібний скрип
subtitle-utterance-goat-calm = Коза мекає
subtitle-utterance-pig-calm = Свиня нюхає
subtitle-utterance-sheep-calm = Вівця бекає
subtitle-utterance-truffler-calm = Трюфелер нюхає
subtitle-utterance-human-greeting = Привітання
subtitle-utterance-adlet-hurt = Адлет ниє
subtitle-utterance-antelope-hurt = Антилопа плаче
subtitle-utterance-biped_large-hurt = Сильно боляче
subtitle-utterance-human-hurt = Людині боляче
subtitle-utterance-lion-hurt = Лев ричить
subtitle-utterance-mandroga-hurt = Крик Мандрагори
subtitle-utterance-maneater-hurt = Людожер відригує
subtitle-utterance-marlin-hurt = Марлін страждає
subtitle-utterance-mindflayer-hurt = Винищувачу розуму боляче
subtitle-utterance-dagon-hurt = Дагон страждає
subtitle-utterance-asp-angry =  Асп шипить
subtitle-utterance-asp-calm = Крякання Аспа
subtitle-utterance-asp-hurt = Аспу боляче
subtitle-utterance-wendigo-angry = Вендіго кричить
subtitle-utterance-wendigo-calm = Вендіго бурмотить
subtitle-utterance-wolf-angry = Вовк гарчить
subtitle-utterance-wolf-hurt = Вовк скиглить
subtitle-utterance-wyvern-angry = Віверна реве
subtitle-utterance-wyvern-hurt = Віверні боляче