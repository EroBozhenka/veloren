hud-do_not_show_on_startup = Не показувати після запуску
hud-show_tips = Показувати підказки
hud-quests = Завдання
hud-you_died = Вам гаплик
hud-waypoint_saved = Контрольну точку збережено
hud-sp_arrow_txt = ОУ
hud-inventory_full = Інвентар повний
hud-someone_else = комусь іншому
hud-another_group = іншій групі
hud-owned_by_for_secs = Належить { $name } протягом { $secs } секунд
hud-press_key_to_show_keybindings_fmt = [{ $key }] Елементи керування
hud-press_key_to_toggle_lantern_fmt = [{ $key }] Ліхтар
hud-press_key_to_show_debug_info_fmt = Натисніть { $key } для відображення технічної інформації
hud-press_key_to_toggle_keybindings_fmt = Натисніть { $key } для відображення елементів керування
hud-press_key_to_toggle_debug_info_fmt = Натисніть { $key } для відображення технічної інформації
hud_items_lost_dur = Ваші споряджені предмети втратили міцність.
hud-press_key_to_respawn = Натисніть { $key }, щоб відновитись біля останньої відвіданого вогнища.
hud-tutorial_btn = Туторіал
hud-tutorial_click_here = Натисни [ { $key } ] щоб мати змогу тицьнути цю кнопку!
hud-tutorial_elements = Ремесло
hud-temp_quest_headline = Вітання, Мандрівник!
hud-temp_quest_text =
    Щоб розпочати свою подорож, можете погуляти по цьому селищу і зібрати собі припасів.
    
    Не соромтесь, беріть все, що вам знадобиться на Шляху.
    
    Подивиться у нижній правий кут свого екрану, там Ви знайдете різні корисні речі як-то сумка, меню ремесла і мапу.
    
    Крафтові станції дозволяють створювати броню, зброю, їжу та багато іншого!
    
    Дикі тварини по всій окрузі є чудовим джерелом Шкіри, щоб створити певний захист від небезпек світу.
    
    Коли ви будете готові, спробуйте отримати ще краще спорядження з численних викликів, позначених на вашій карті!
hud-spell = Чари
hud-diary = Щоденник
hud-free_look_indicator = Вільний погляд ввімкнено. Натисніть { $key }, щоб вимкнути.
hud-camera_clamp_indicator = Камера вертикально закріплена. Натисніть { $key } щоб відімкнути.
hud-auto_walk_indicator = Авто Рух
hud-zoom_lock_indicator-remind = Зум заблоковано
hud-zoom_lock_indicator-enable = Збільшення камери заблоковано
hud-zoom_lock_indicator-disable = Збільшення камери розблоковано
hud-collect = Зібрати
hud-pick_up = Підібрати
hud-open = Відкрити
hud-use = Використати
hud-read = Читати
hud-unlock-requires = Відкрити за допомогою { $item }
hud-unlock-consumes = Використовуйте { $item } щоб відкрити
hud-mine = Добути
hud-mine-needs_pickaxe = Потрібне Кайло
hud-mine-needs_unhandled_case = Потрібно ???
hud-talk = Потеревенити
hud-trade = Поторгувати
hud-mount = Осідлати
hud-sit = Сидіти
hud-steer = Керувати
