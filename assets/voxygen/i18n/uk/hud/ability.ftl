common-abilities-debug-possess = Заклинаюча Стріла
    .desc = Стріляє отруйною стрілою. Дає тобі контроль над ціллю.
common-abilities-hammer-leap = Погром
    .desc = Атака по області. Стрибок спрямований за курсором.
common-abilities-bow-shotgun = Стріловик
    .desc = Постріл купою стріл за раз.
common-abilities-staff-fireshockwave = Кільце Вогню
    .desc = Підпалює землю вогненною ударною хвилєю.
common-abilities-sceptre-wardingaura = Захисна Аура
    .desc = Укріплює тебе і твоїх спільників силами природи на деякий час.
# Sword abilities
veloren-core-pseudo_abilities-sword-heavy_stance = Важка позиція
    .desc = Атаки в цій стійці можуть збивати з рівноваги ворогів і завдати їм більше шкоди, але повільніше
veloren-core-pseudo_abilities-sword-agile_stance = Легка позиція
    .desc = У цій позиції атаки швидші, але менш потужні
veloren-core-pseudo_abilities-sword-defensive_stance = Оборонна позиція
    .desc = Атаки в цій стойці можуть виступати або як слабкий блок, або як відбивання
veloren-core-pseudo_abilities-sword-crippling_stance = Калічача позиція
    .desc = Атаки в цій стійкі створюють або погіршують тривалі травми
veloren-core-pseudo_abilities-sword-cleaving_stance = Позиція Рубання
    .desc = Атаки в цій позиції можуть поранити декількох ворогів
veloren-core-pseudo_abilities-sword-double_slash = Подвійний удар
    .desc = Комбінація з двох ударів.
common-abilities-sword-basic_double_slash = Базовий подвійний удар
    .desc = Базова комбінація з двох ударів.
common-abilities-sword-heavy_double_slash = Важкий подвійний удар
    .desc = Повільніша комбінація з двох ударів, яка може вивести ворогів з рівноваги
common-abilities-sword-agile_double_slash = Худкий подвійний удар
    .desc = Швидка комбінація з двох ударів із легкими ударами
common-abilities-sword-defensive_double_slash = Оборонний подвійний удар
    .desc = Комбо з двох ударів, яке зменшує вплив ворожих ударів
common-abilities-sword-crippling_double_slash = Глибокий подвійний удар
    .desc = Комбо з двох ударів, яке може продовжити кровотечу ворога
common-abilities-sword-cleaving_double_slash = Розсікаючий подвійний  удар
    .desc = Комбо з двох ударів, яке може пробити кількох ворогів
veloren-core-pseudo_abilities-sword-secondary_ability = Вторинна здатність меча
    .desc = Здатність, прив’язана до вторинного ключа атаки
common-abilities-sword-basic_thrust = Базовий поштовх
    .desc = Зарядка тяги зробить її більш потужною
common-abilities-sword-heavy_slam = Важкий удар
    .desc = Сильний слеш над головою, який можна зарядити для більш збиваючої дії
common-abilities-sword-agile_perforate = Продірявлювати
    .desc = Стрімкий шквал легких атак
common-abilities-sword-agile_dual_perforate = Подвійний Продірявлувач
    .desc = Стрімкий шквал легких атак обома мечами
common-abilities-sword-defensive_vital_jab = Захисний Віталь Джеб
    .desc = Швидкий удар, який завдає більше шкоди  від відбитих ворогів
common-abilities-sword-crippling_deep_rend = Глибокий розрив
    .desc = Удар, спрямований у вже відкриту рану, завдає більше шкоди ворогам, що кровоточать
common-abilities-sword-cleaving_spiral_slash = Спіральний удар
    .desc = Проведіть своїм лезом повністю навколо себе, щоб вразити будь-кого поблизу
common-abilities-sword-cleaving_dual_spiral_slash = Подвійний спіральний удар
    .desc = Обведіть обидва леза навколо себе, щоб вразити будь-кого поблизу
veloren-core-pseudo_abilities-sword-crescent_slash = Півмісяць
    .desc =
        Діагональний удар вверх
        Змінюється залежно від вашої позиції
common-abilities-sword-basic_crescent_slash = Базовий Півмісяць
    .desc = Базовий діагональний удар вверх
common-abilities-sword-heavy_crescent_slash = Важкий Півмісяць
    .desc = Діагональний удар вверх який може збити з ніг
common-abilities-sword-agile_crescent_slash = Хуткий Півмісяць
    .desc = Легкий діагональний удар вверх
common-abilities-sword-defensive_crescent_slash = Оборонний Півмісяць
    .desc = Відбиваючий, діагональний удар вгору
common-abilities-sword-crippling_crescent_slash = Глибокий півмісяць
    .desc = Діагональний удар вгору, який може викликати кровотечу
common-abilities-sword-cleaving_crescent_slash = Розсікаючий Півмісяць
    .desc = Діагональний удар вгору, який може розбивати ворогів
veloren-core-pseudo_abilities-sword-fell_strike = Смертоносний удар
    .desc =
        Швидкий сильний удар
        Змінюється залежно від вашої позиції
common-abilities-sword-basic_fell_strike = Базовий Смертоносний удар
    .desc = Базовий швидкий сильний удар
common-abilities-sword-heavy_fell_strike = Важкий Смертоносний удар
    .desc = Швидкий сильний удар який може збити з ніг
common-abilities-sword-agile_fell_strike = Хуткий Смертоносний удар
    .desc = Дуже швидкий і сильний удар
common-abilities-sword-defensive_fell_strike = Оборонний Смертоносний удар
    .desc = Відбиваючий швидкий сильний удар
common-abilities-sword-crippling_fell_strike = Глибокий Смертоносний удар
    .desc = Швидкий сильний удар який може викликати кровотечу
common-abilities-sword-cleaving_fell_strike = Розсікаючи Смертоносний удар
    .desc = Швидкий сильний удар, який може пробити ворогів
veloren-core-pseudo_abilities-sword-skewer = Проколювач
    .desc =
        Колючий випад
        Змінюється в залежності від вашої позиції
common-abilities-sword-basic_skewer = Базовий Проколювач
    .desc = Основний, колючий випад
common-abilities-sword-heavy_skewer = Важкий Проколювач
    .desc = Колючий випад, який може похитнути
common-abilities-sword-agile_skewer = Хуткий Проколювач
    .desc = Швидкий, колючий випад
common-abilities-sword-defensive_skewer = Оборонний Проколювач
    .desc = Відбиваючий колючий випад
common-abilities-sword-crippling_skewer = Глибокий Проколювач
    .desc = Колючий випад який може викликати кровотечу
common-abilities-sword-cleaving_skewer = Розсікаючи Проколювач
    .desc = Колючий випад, який може пробити ворогів
veloren-core-pseudo_abilities-sword-cascade = Каскад
    .desc =
        Удар зверху
        Змінюється в залежно від вашої позиції
common-abilities-sword-basic_cascade = Базовий Каскад
    .desc = Основний удар зверху
common-abilities-sword-heavy_cascade = Важкий Каскад
    .desc = Удар зверху, який може похитнути
common-abilities-sword-agile_cascade = Хуткий Каскад
    .desc = Швидкий, удар зверху
common-abilities-sword-defensive_cascade = Оборонний Каскад
    .desc = Відбиваючий удар зверху
common-abilities-sword-crippling_cascade = Глибокий Каскад
    .desc = Удар зверху який може викликати кровотечу
common-abilities-sword-cleaving_cascade = Розсікаючи Каскад
    .desc = Удар зверху який може пробити ворогів
veloren-core-pseudo_abilities-sword-cross_cut = Перехресний розріз
    .desc =
        Правий та лівий розріз
        Змінюється в залежності від вашої позиції
common-abilities-sword-basic_cross_cut = Базовий Перехресний розріз
    .desc = Основний правий та лівий розріз
common-abilities-sword-heavy_cross_cut = Важкий Перехресний розріз
    .desc = Правий та лівий розріз, який може похитнути
common-abilities-sword-agile_cross_cut = Хуткий Перехресний розріз
    .desc = Швидкий правий та лівий розріз
common-abilities-sword-defensive_cross_cut = Оборонний Перехресний розріз
    .desc = Відбиваючий правий та лівий розріз
common-abilities-sword-crippling_cross_cut = Глибокий Перехресний розріз
    .desc = Правий та лівий розріз, які можуть викликати кровотечу
common-abilities-sword-cleaving_cross_cut = Розсікаючи Перехресний розріз
    .desc = Правий та лівий розріз який може пробити ворогів
common-abilities-sword-basic_dual_cross_cut = Базовий Перехресний розріз
    .desc = Одночасний основний правий та лівий розріз
common-abilities-sword-heavy_dual_cross_cut = Важкий Перехресний розріз
    .desc = Одночасний правий та лівий розріз, який може похитнути
common-abilities-sword-agile_dual_cross_cut = Хуткий Перехресний розріз
    .desc =  Одночасний швидкий правий та лівий розріз
common-abilities-sword-defensive_dual_cross_cut = Оборонний Перехресний розріз
    .desc = Одночасний відбиваючий правий та лівий розріз
common-abilities-sword-crippling_dual_cross_cut = Глибокий Перехресний розріз
    .desc = Одночасний правий та лівий розріз, які можуть викликати кровотечу
common-abilities-sword-cleaving_dual_cross_cut = Розсікаючи Перехресний розріз
    .desc = Одночасний правий та лівий розріз який може пробити ворогів
veloren-core-pseudo_abilities-sword-finisher = Завершувач
    .desc =
        Вміння, яке використовує комбо і призначене для завершення бою
        Завершальний удар буде різним залежно від вашої позиції
common-abilities-sword-basic_mighty_strike = Могутній удар
    .desc =
        Простий, потужний удар
        Потребує помірної кількості комбо для використання
common-abilities-sword-heavy_guillotine = Гільйотина
    .desc =
        Сильний розріз, який, ймовірно, вивергне те, що не вб'є
        Потребує помірної кількості комбо для використання
common-abilities-sword-agile_hundred_cuts = Сотня порізів
    .desc =
        Багато дуже швидких ударів по цілі
        Для використання потрібна помірна кількість комбо
common-abilities-sword-defensive_counter = Контратака
    .desc =
        Швидка атака, яка завдає значно більшої шкоди відбитому противнику
        Для використання потрібна помірна кількість комбо
common-abilities-sword-crippling_mutilate = Понівечення
    .desc =
        Понівечте свого ворога, розпилявши його поранення, завдаючи більше шкоди ворогам, що кровоточать
        Для використання потрібна помірна кількість комбо
common-abilities-sword-cleaving_bladestorm = Буря клинків
    .desc =
        Знищуйте своїх ворогів кількома циклічними помахами меча
        Для використання потрібна помірна кількість комбо
common-abilities-sword-cleaving_dual_bladestorm = Подвійна Буря клинків
    .desc =
        Знищуйте своїх ворогів кількома циклічними помахами обома вашими мечами
        Для використання потрібна помірна кількість комбо
common-abilities-sword-heavy_sweep = Важкий замах
    .desc =
        Сильний, широкий, розмашистий удар, який завдає більшої шкоди ворогові, після дестабілізації
        Займає важку позу
common-abilities-sword-heavy_pommel_strike = Удар навершям
    .desc =
        Потрясіть ворога тупим ударом по голові
        Входить у важку позу
common-abilities-sword-agile_quick_draw = Швидкий випад
    .desc =
        Киньтеся вперед, виймаючи лезо для швидкої атаки
        Входить в спритну стійку
common-abilities-sword-agile_feint = Фінт
    .desc =
        Зробіть крок убік, а потім назад перед ударом
        Входить в спритну стійку
common-abilities-sword-defensive_riposte = Репост
    .desc =
        Паріюйте удар перед миттєвою контратакою
        Займає оборонну 
common-abilities-sword-defensive_disengage = Відступ
    .desc =
        Відступіть на крок назад після удару
        Займає оборонну позицію
common-abilities-sword-crippling_gouge = Довбач
    .desc =
        Завдайте вашому ворогові велику рану, яка буде продовжувати кровоточити
        Входить у позицію "ослаблення"
common-abilities-sword-crippling_hamstring = Сухожиллеріз
    .desc =
        Пошкодьте сухожилля вашого ворога, зробивши його менш рухливим
        Входить у позицію "ослаблення"
common-abilities-sword-cleaving_whirlwind_slice = Вихор розрізу
    .desc =
        Вражайте всіх оточуючих ворогів круговими атаками
        Входить у позицію "розсічення"
common-abilities-sword-cleaving_dual_whirlwind_slice = Вихори розрізу
    .desc =
        Вдаряйте всіх оточуючих ворогів круговими атаками, використовуючи обидва свої мечі
        Входить у позицію "розсічення"
common-abilities-sword-cleaving_earth_splitter = Розкол Землі
    .desc =
        Розколоти землю, якщо використовувати під час падіння, це матиме набагато сильніший удар
        Входить у позицію "розсічення"
common-abilities-sword-heavy_fortitude = Стійкість духу
    .desc =
        Збільшує стійкість до дестабілізації і зі збільшенням отриманих пошкоджень ваші атаки будуть збивати з рівноваги ворогів
        Вимагає важкої позиції
common-abilities-sword-heavy_pillar_thrust = Гравтаційний стовп
    .desc =
        Пронизуйте своїм мечем супротивника, до кінця в землю, потужніший, якщо використовувати під час падіння
        Вимагає важкої стійки
common-abilities-sword-agile_dancing_edge = Танцюче гостре лезо
    .desc =
        Рухайтесь і атакуйте швидше
        Вимагає спритної стійки
common-abilities-sword-agile_flurry = Шквал
    .desc =
        Багаторазові швидкі удари
        Вимагає спритної стійки
common-abilities-sword-agile_dual_flurry = Мегашквал
    .desc =
        Кілька швидких ударів обома мечами
        Вимагає спритної стійки
common-abilities-sword-defensive_stalwart_sword = Вірний меч
    .desc =
        Відкидайте основну масу атак, вхідні пошкодження зменшуються
        Вимагає захисної позиції
common-abilities-sword-defensive_deflect = Відбивання
    .desc =
        Достатньо швидкий маневр навіть для блокування снарядів
        Вимагає захисної позиції
common-abilities-sword-crippling_eviscerate = Випотрошувач
    .desc =
        Розірвання ран подальше, завдає більше шкоди ослабленим ворогам
        Вимагає позиції "ослаблення"
common-abilities-sword-crippling_bloody_gash = Кривавий поріз
    .desc =
        Жорстоко вдаряючи по вже кровоточачим ранам, завдає більше шкоди ворогам, що кровоточать
        Вимагає позиції "ослаблення"
common-abilities-sword-cleaving_blade_fever = Лихоманка леза
    .desc =
        Атакуйте більш безрозсудно, збільшуючи силу ваших ударів, але відкриваючи себе для вхідних атак
        Вимагає позиції "розсічення"
common-abilities-sword-cleaving_sky_splitter = Розділювач небес
    .desc =
        Потужний удар, який нібито може навіть розколоти небо, але розколе ворогів
        Потрібна стійка на розсічення

# Axe abilities
common-abilities-axe-triple_chop = Потрійна відбивна
    .desc =
       Три швидких удари
common-abilities-axe-cleave = Розкол
    .desc =
        Розбивка вниз, яка може створювати кілька комбінацій
common-abilities-axe-brutal_swing = Брутальний свінг
    .desc =
        Навколо тебе обертовий розкол
common-abilities-axe-berserk = Берсерк
    .desc =
        Збільшує вашу силу ціною того, що ви залишаєтеся вразливими
common-abilities-axe-rising_tide = Наростаюча хвиля
    .desc =
        Удар угору, який значно збільшує комбінацію
common-abilities-axe-savage_sense = Дике почуття
    .desc =
        Визначте життєво важливу точку на вашій цілі, щоб ваш наступний удар завдав їй критичної шкоди
common-abilities-axe-adrenaline_rush = Адреналіновий Раш
    .desc =
        Споживайте всю свою комбінацію, щоб поповнити витривалість
        Змінюється з комбінацією при активації, споживає всю комбінацію
common-abilities-axe-execute = Виконавець
    .desc =
        Нищівний удар, який часто буває смертельним
        Для використання потрібно 30 комбо
        Автоматично оновлюється до виру при 50 комбо, якщо його розблоковано
common-abilities-axe-maelstrom = Водоворіт
    .desc =
        Вдаряйте все, що знаходиться поблизу, нищівним обертовим ударом
        Автоматично оновлюється з комбінації «Виконати при 50».
common-abilities-axe-rake = Граблі
    .desc =
        Проведіть сокирою по своєму ворогу, викликаючи кровотечу
common-abilities-axe-bloodfeast = Криваве свято
    .desc =
        Ваша сокира жадає крові ваших ворогів, поповнюючи вас з кожним ударом по скривавленому ворогу
common-abilities-axe-fierce_raze = Жорстоке Знищення
    .desc =
        Стрімкий шквал ударів по вашому ворогу
common-abilities-axe-dual_fierce_raze = Люте Знищення
    .desc =
        Стрімкий шквал ударів по вашому ворогу з використанням обох ваших сокир
common-abilities-axe-furor = Фурор
    .desc =
        Коли ваша лють зростає, ваші удари генерують більше комбо
common-abilities-axe-fracture = Перелом
    .desc =
        Ослаблюючий удар, який обмежує рух ворога
        Змінюється з комбінацією при активації, споживає половину комбінації
common-abilities-axe-lacerate = Лацерувати
    .desc =
        Виріжте свою ціль, викликаючи вивід їхнього життєвого соку
        Вимагає 30 комбо для використання
        Автоматично підвищується до Обезкровлення на 50 комбо, якщо відкрито
common-abilities-axe-riptide = Обезкровлення
    .desc =
        Виріжте все навколо, розділяючи їхню кров
        Автоматично підвищується з роздирати на 50 комбо
common-abilities-axe-skull_bash = Удар лобом
    .desc =
        Удар плоскою стороною вашої сокири, який може зупинити ворога
common-abilities-axe-sunder = Роздерач
    .desc =
        Змінивши хватку, ви здатні обходити броню ворога, одночасно ефективніше відновлюючи свою енергію
common-abilities-axe-plunder = Грабіж
    .desc =
        Швидко підійдіть до вашого ворога, позбавивши його рівноваги ударом
common-abilities-axe-defiance = Непокора
    .desc =
        Дивіться смерті в очі довше, роблячи себе стійким до стагнацій і смерті
common-abilities-axe-keelhaul = Рибак
    .desc =
        Зачепіть вашого суперника, щоб притягнути його ближче до себе
        Масштабується з комбо при активації, використовує половину комбо
common-abilities-axe-bulkhead = Брудна Стіна
    .desc =
        Важкий удар, який, за деякими чутками, навіть може зупинити титанів
        Вимагає 30 комбо для використання
        Автоматично підвищується до Перекидання на 50 комбо, якщо розблоковано
common-abilities-axe-capsize = Перекидання
    .desc =
        Зупиняйте все навколо себе важким обертовим ударом
        Автоматично підвищується з Брудної Стіни на 50 комбо
