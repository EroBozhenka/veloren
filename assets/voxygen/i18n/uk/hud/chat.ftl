## Player events
hud-chat-online_msg = [{ $name }] зараз онлайн
hud-chat-offline_msg = [{ $name }] не в мережі
## Buff outcomes
hud-outcome-burning = помер від:палаючої дупи
hud-outcome-curse = помер від:прокльону
hud-outcome-bleeding = помер від:кровотечі
hud-outcome-crippled = помер від:каліцтва
hud-outcome-frozen = помер від:переохолодження
hud-outcome-mysterious = помер від:дивного нічого
## Buff deaths
hud-chat-died_of_pvp_buff_msg = [{ $victim }] { $died_of_buff } викликано [{ $attacker }]
hud-chat-died_of_buff_nonexistent_msg = [{ $victim }] { $died_of_buff }
hud-chat-died_of_npc_buff_msg = [{ $victim }] { $died_of_buff } викликано { $attacker }
## PvP deaths
hud-chat-pvp_melee_kill_msg = [{ $attacker }] переміг [{ $victim }]
hud-chat-pvp_ranged_kill_msg = [{ $attacker }] вистрелив в [{ $victim }]
hud-chat-pvp_explosion_kill_msg = [{ $attacker }] підірвав [{ $victim }]
hud-chat-pvp_energy_kill_msg = [{ $attacker }] вбив [{ $victim }] магією
hud-chat-pvp_other_kill_msg = [{ $attacker }] вбив [{ $victim }]
## PvE deaths
hud-chat-npc_melee_kill_msg = { $attacker } вбив [{ $victim }]
hud-chat-npc_ranged_kill_msg = { $attacker } вистрелив [{ $victim }]
hud-chat-npc_explosion_kill_msg = { $attacker } підірвав [{ $victim }]
hud-chat-npc_energy_kill_msg = { $attacker } вбив [{ $victim }] магією
hud-chat-npc_other_kill_msg = { $attacker } вбив [{ $victim }]
## Other deaths
hud-chat-environmental_kill_msg = [{ $name }] помер в { $environment }
hud-chat-fall_kill_msg = [{ $name }] не вміє літати
hud-chat-suicide_msg = [{ $name }] закосплеїв Курта Кабейна
hud-chat-default_death_msg = [{ $name }] загинув
## Utils
hud-chat-all = Усі
hud-chat-you = Ти
hud-chat-chat_tab_hover_tooltip = Клацніть правою кнопкою миші для налаштувань
hud-loot-pickup-msg = {$actor} підняв { $amount ->
   [one] { $item }
   *[other] {$amount}x {$item}
}
hud-chat-loot_fail = Твої сумки і кишені переповнені!
hud-chat-goodbye = Допобачення!
hud-chat-connection_lost = З'єднання втрачено. Виконується відключення через { $time } секунд.