hud-quest = Завдання
hud-quest-intro = Вітаю, { $playername }!
hud-quest-desc-fetch = Будь ласка, допоможіть мені знайти:
hud-quest-desc-kill = Чи ви можете мені допомогти вбити
hud-quest-reward = Я винагороджу вас:
hud-quest-accept = Прийняти
hud-quest-decline = Відхилити