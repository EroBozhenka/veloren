## Regeneration
buff-title-heal = Зцілення
buff-desc-heal = Поступово відновлює Здоров'я.
buff-stat-health = Відновлює { $str_total } ОЗ
## Potion
buff-title-potion = Зілля
buff-desc-potion = Пиття...
## Saturation
buff-title-saturation = Насичення
buff-desc-saturation = Поступово відновлює Здоров'я з їжі.
## Campfire
buff-title-campfire_heal = Відновлення біля вогнища
buff-desc-campfire_heal = Відпочинок біля вогнища лікує на { $rate }% за секунду.
## Energy Regen
buff-title-energy_regen = Відновлення Енергії
buff-desc-energy_regen = Пришвидшене відновлення Енергії
buff-stat-energy_regen = Відновлює { $str_total } енергії
## Health Increase
buff-title-increase_max_health = Підвищення Максимального Здоров'я
buff-desc-increase_max_health = Піднімає ліміт вашого здоров'я
buff-stat-increase_max_health =
    Підвищує максимальне здоров'я
    на { $strength }
## Energy Increase
buff-title-increase_max_energy = Підвищення Максимальної Енергії
buff-desc-increase_max_energy = Піднімає ліміт вашої енергії
buff-stat-increase_max_energy =
    Підвищує максимальну енергію
    на { $strength }
## Invulnerability
buff-title-invulnerability = Невразливість
buff-desc-invulnerability = Ви невразливий, тільки тримайтесь подалі від омели.
buff-stat-invulnerability = Дає невразливість
## Protection Ward
buff-title-protectingward = Захисна Аура
buff-desc-protectingward = Ви захищені від атак, у якомусь сенсі.
## Frenzied
buff-title-frenzied = Манія
buff-desc-frenzied = Кров тече швидше, прискоруючи ваш рух та помалу зцілюючи вас.
## Haste
buff-title-hastened = Квапливість
buff-desc-hastened = Ваша швидкість переміщення і шкидкість атак прискорена.
## Bleeding
buff-title-bleed = Кровотеча
buff-desc-bleed = Завдає регулярних пошкодженнь.
## Curse
buff-title-cursed = Проклін
buff-desc-cursed = Вас прокляли.
## Burning
buff-title-burn = У Вогні
buff-desc-burn = Ви згораєте заживо.
## Crippled
buff-title-crippled = Калічення
buff-desc-crippled = Ваші рухи дуже скуті через отримані травми.
## Freeze
buff-title-frozen = Обмороження
buff-desc-frozen = Швидкість пересування та атак знижена.
## Wet
buff-title-wet = Волога
buff-desc-wet = Земля плутає ваші ноги ускладнючи пересування.
## Ensnared
buff-title-ensnared = Пастка
buff-desc-ensnared = Ліани опутують ваші ноги, перешкоджаючи ходьбі.
## Fortitude
buff-title-fortitude = Стійкість
buff-desc-fortitude = Ви можете витримувати потрясіння, при отриманні шкоди ви легше дестабілізуєте ворога.
## Parried
buff-title-parried = Відбитий 
buff-desc-parried = Ваш замах відбито, тепер ви повільно відновлюєтесь.
## Potion sickness
buff-title-potionsickness = Зільна хвороба
buff-desc-potionsickness = Зілля лікують вас менше після недавнього їх вживання.
buff-stat-potionsickness =
    Зменшує кількість відновлення здоров'я
    від подальших зілля на { $strength }%.
## Reckless
buff-title-reckless = Нерозважливий
buff-desc-reckless = Ваші атаки потужніші, але ви відкриті для атак.
## Polymorped
buff-title-polymorphed = Поліморфія
buff-desc-polymorphed = Ваше тіло змінює форму.
## Flame
buff-title-flame = Вогонь
buff-desc-flame = Вогонь - ваш союзник.
## Frigid
buff-title-frigid = Холодний
buff-desc-frigid = Заморозьте своїх ворогів.
## Lifesteal
buff-title-lifesteal = Вампір
buff-desc-lifesteal = Висмоктуйте життя зі своїх ворогів.
## Salamander's Aspect
buff-title-salamanderaspect = Аспект Саламандри
buff-desc-salamanderaspect = Ви не горите у вогні та швидко рухаєтесь в лаві.
## Imminent Critical
buff-title-imminentcritical = Неминучий удар
buff-desc-imminentcritical = Ваша наступна атака завдасть критичного удару ворогу.
## Fury
buff-title-fury = Лють
buff-desc-fury = З вашим гнівом ваші удари генерують більше комбо.
## Sunderer
buff-title-sunderer = Руйнівник
buff-desc-sunderer = Ваші атаки можуть пробити оборону ворогів і наповнити вас більшою кількістю енергії.
## Sunderer
buff-title-defiance = Непокора
buff-desc-defiance = Ви можете витримувати потужніші удари та генерувати комбо, коли вас б'ють, але ви повільніше рухаєтесь.
## Bloodfeast
buff-title-bloodfeast = Кривавий Бенкет
buff-desc-bloodfeast = Відновлюєте життя під час атак на ворогів з кровотечею.
## Berserk
buff-title-berserk = Берсерк
buff-desc-berserk = Ви перебуваєте в шаленій люті, що робить ваші атаки потужнішими і швидкими. А також збільшує вашу швидкість. Однак у результаті ваш захист менший.
## Util
buff-text-for_seconds = протягом { $dur_secs } сек.
buff-text-over_seconds = впродовж { $dur_secs } сек.
buff-mysterious = Таємничий ефект
buff-remove = Клікніть, щоб видалити
